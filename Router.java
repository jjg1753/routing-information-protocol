
/*
This program is used to simulate the job of a Router in a
DVR algorithm

Author : Jeet Gandhi(jjg1753)
 */

import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.*;

public class Router extends Thread {
    private RoutingTable rTable;
    private static int listenerPort = 8072;
    private List<String[]> rowsInConfigFile = new ArrayList<>();

    public static void main(String cLineArguments[]) throws IOException {
        System.out.println(System.getProperty("user.dir"));
        File configFile = new File("C:\\Users\\jeetd\\Desktop\\Spring2018_docs\\iri-dev\\FCN\\src\\config_c.txt");
        File non_neighbour = new File("C:\\Users\\jeetd\\Desktop\\Spring2018_docs\\iri-dev\\FCN\\src\\non_neighbour_c.txt");
        BufferedReader bReader = new BufferedReader(new FileReader(configFile));
        BufferedReader bReaderNonNeighbour = new BufferedReader(new FileReader(non_neighbour));
        String st;
        String ip;
        String[] tempArray, tempArrayNonNeighbours;
        Router thisRouter = new Router();

        List<String[]> rowsInNonNeighbour = new ArrayList<>();

        /* Try block to get the IP address of the Router */
        try(final DatagramSocket socket = new DatagramSocket()){
            socket.connect(InetAddress.getByName("8.8.8.8"), 10002);
            ip = socket.getLocalAddress().getHostAddress();
        }

        while ((st = bReader.readLine()) != null) {
            tempArray = st.split(":");
            thisRouter.rowsInConfigFile.add(tempArray);
        }
        thisRouter.rTable = new RoutingTable(thisRouter.rowsInConfigFile, ip);

        while ((st = bReaderNonNeighbour.readLine()) != null){
            tempArrayNonNeighbours = st.split(":");
            rowsInNonNeighbour.add(tempArrayNonNeighbours);
        }
        thisRouter.rTable.addNonNeighbourInRoutingTable(rowsInNonNeighbour, ip);

        thisRouter.rTable.printRTable(ip);
        Thread listenerThread = new Thread(() -> {
            System.out.println("Listening on port :"+listenerPort);
            DatagramSocket ds = null;
            try {
                ds = new DatagramSocket(listenerPort);
                byte[] receivedData = new byte[65535];
                while (true){
                    DatagramPacket incomingPacket = new DatagramPacket(receivedData, receivedData.length);
                    ds.receive(incomingPacket);
                    byte[] data = incomingPacket.getData();
                    ByteArrayInputStream in = new ByteArrayInputStream(data);
                    ObjectInputStream is = new ObjectInputStream(in);
                    try {
                        RoutingTable receivedPacket = (RoutingTable) is.readObject();
                        System.out.println("\n\n\n Routing Table Received from -> "+receivedPacket.getOwnerOfRoutingTable());
                        receivedPacket.printRTable(receivedPacket.getOwnerOfRoutingTable());
                        HashMap<String, List<RoutingTableRow>> updatedRoutingTable = handleReceivedRoutingTable(receivedPacket, thisRouter.rTable);
                        thisRouter.rTable.setrTable(updatedRoutingTable);

                        System.out.println("Updated Routing table at -> "+ ip );
                        thisRouter.rTable.printRTable(ip);
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            } catch (IOException e) {
                System.out.println("Idhar error aaaya !");
                e.printStackTrace();
            }finally {
                System.out.println("Finally Block Executed.!");
                Objects.requireNonNull(ds).close();
            }
        });
        listenerThread.start();
        thisRouter.start();

    }

    private static HashMap<String, List<RoutingTableRow>> handleReceivedRoutingTable(RoutingTable receivedPacket, RoutingTable thisRouter) {
        HashMap<String, List<RoutingTableRow>> receivedRT = receivedPacket.getrTable();
        HashMap<String, List<RoutingTableRow>> thisRouterRT = thisRouter.getrTable();
        for (Map.Entry<String, List<RoutingTableRow>> eachRouter : receivedRT.entrySet()) {
            String routerIP = eachRouter.getKey();
            RoutingTableRow RTRow = eachRouter.getValue().get(0);

            if(thisRouterRT.containsKey(RTRow.getDestinationIP())){
                if(!Objects.equals(RTRow.getDestinationIP(), thisRouter.getOwnerOfRoutingTable())){

                    int newCost = thisRouterRT.get(receivedPacket.getOwnerOfRoutingTable()).get(0).getCostToGoal() + RTRow.getCostToGoal();
                    if(newCost < thisRouterRT.get(RTRow.getDestinationIP()).get(0).getCostToGoal() && newCost <= 16){
                        thisRouterRT.get(routerIP).get(0).setCostToGoal(newCost);
                        thisRouterRT.get(routerIP).get(0).setNextHop(receivedPacket.getOwnerOfRoutingTable());
                    }
                }
            }else{
                if(!Objects.equals(RTRow.getDestinationIP(), thisRouter.getOwnerOfRoutingTable())) {

                    thisRouterRT.put(routerIP, Collections.singletonList(new RoutingTableRow(thisRouter.getOwnerOfRoutingTable(),
                            routerIP, receivedPacket.getOwnerOfRoutingTable(), String.valueOf(RTRow.getCostToGoal() +
                            thisRouterRT.get(receivedPacket.getOwnerOfRoutingTable()).get(0).getCostToGoal()), String.valueOf(RTRow.getPort()))));
                }
            }
        }
        return thisRouterRT;    //Updated routing table returned for the current router
    }

    @Override
    public void run() {
        while(true){
            try {
                sleep(10000);
                DatagramSocket ds = new DatagramSocket();
                ByteArrayOutputStream bStream = new ByteArrayOutputStream();
                ObjectOutputStream oo = new ObjectOutputStream(bStream);
                oo.writeObject(rTable);

                byte[] serializedMessage = bStream.toByteArray();
                for(String[] row : rowsInConfigFile){
                    DatagramPacket DpSend =
                            new DatagramPacket(serializedMessage, serializedMessage.length, InetAddress.getByName(row[0]),
                                    Integer.parseInt(row[1]));
                    ds.send(DpSend);
                    System.out.println("Packet sent to : "+ row[0] + " on Port no. : "+ row[1]);
                }
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}


