
import java.io.Serializable;
import java.util.*;

class RoutingTable implements Serializable {

    private String OwnerOfRoutingTable;
    private HashMap<String, List<RoutingTableRow>> rTable = new HashMap<>();
    private boolean shutDown;

    boolean isShutDown() {
        return shutDown;
    }

    void setShutDown(boolean shutDown) {
        this.shutDown = shutDown;
    }

    String getOwnerOfRoutingTable() {
        return OwnerOfRoutingTable;
    }


    RoutingTable(List<String[]> rowsInConfigFile, String ip) {
        this.OwnerOfRoutingTable = ip;
        for(String[] row : rowsInConfigFile)
            rTable.put(row[0], Collections.singletonList(new RoutingTableRow(ip, row[0], row[0], row[2], row[1])));

    }

    HashMap<String, List<RoutingTableRow>> getrTable() {
        return rTable;
    }

    public void setrTable(HashMap<String, List<RoutingTableRow>> rTable) {
        this.rTable = rTable;
    }

    void addNonNeighbourInRoutingTable(List<String[]> nonNeighbours, String ip){
        for (String[] row : nonNeighbours)
            rTable.put(row[0], Collections.singletonList(new RoutingTableRow(ip, row[0], null,
                    "100", row[1])));
    }

    void printRTable(String ip){
        System.out.println("*************************************************");
        System.out.println("Routing Table at : " + ip);
        System.out.println("**************************************************");
        for (Map.Entry<String, List<RoutingTableRow>> entry : rTable.entrySet()) {
            for(RoutingTableRow row : entry.getValue()){
                System.out.println("Destination : " + row.getDestinationIP());
                System.out.println("Next Hop : "+ row.getNextHop());
                System.out.println("Cost to the destination : "+ row.getCostToGoal());
                System.out.println("------------------------------------------------");
            }
        }
    }
}

class RoutingTableRow implements Serializable{

    private String sourceIP;
    private String destinationIP;
    private String nextHop;
    private int costToGoal;
    private int port;

    RoutingTableRow(String sourceIP, String destinationIP, String nextHop, String costToGoal, String port) {
        this.sourceIP = sourceIP;
        this.destinationIP = destinationIP;
        this.nextHop = nextHop;
        this.costToGoal = Integer.parseInt(costToGoal);
        this.port = Integer.parseInt(port);
    }

    String getSourceIP() {
        return sourceIP;
    }

    public void setSourceIP(String sourceIP) {
        this.sourceIP = sourceIP;
    }

    String getDestinationIP() {
        return destinationIP;
    }

    public void setDestinationIP(String destinationIP) {
        this.destinationIP = destinationIP;
    }

    String getNextHop() {
        return nextHop;
    }

    public void setNextHop(String nextHop) {
        this.nextHop = nextHop;
    }

    int getCostToGoal() {
        return costToGoal;
    }

    public void setCostToGoal(int costToGoal) {
        this.costToGoal = costToGoal;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}

